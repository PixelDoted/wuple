/// Create drawable text
pub fn text(string: String, scale: f32, color: [f64; 4]) -> Text {
    Text {
        string,
        scale,
        color,
    }
}

/// Drawable Text
#[derive(Clone, Debug)]
pub struct Text {
    pub string: String,
    pub scale: f32,
    pub color: [f64; 4],
}
