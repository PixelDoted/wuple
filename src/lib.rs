//! Simple, Performant rendering on WGPU
//!
//! ## Example
//! ```
//! use wuple::App;
//!
//! pub struct Example {}
//! impl App for Example {
//!     fn frame(&mut self, canvas: &mut wuple::canvas::Canvas, _info: &wuple::info::Info, _first: bool) {
//!         let painter = wuple::painter::Painter::new();
//! 
//!         // Draw a Rectangle
//!         painter.rect(
//!             (-0.5, -0.5), (0.5, 0.5),
//!             wuple::canvas::Draw::Color(wuple::draw::color::WHITE)
//!             canvas,
//!         );
//!     }
//!
//!     fn event(&mut self, event: &winit::event::WindowEvent) -> bool {
//!         // Handle Event
//!         false
//!     }
//! }
//!
//! Example {}.run(true);
//! ```

mod backend;
pub mod canvas;
pub mod draw;
pub mod info;
pub mod painter;
pub mod settings;
pub mod vertex;

use canvas::Canvas;
pub use winit;

use info::Info;

use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use self::backend::State;

/// Result for the event function
#[derive(Default)]
pub enum EventResult {
    #[default]
    Unhandled,
    Handled,
    Exit,
}

/// Main Application trait  
/// handles rendering, inputs and updates
pub trait App {
    /// Window Initialization
    fn init(&mut self, _info: &Info) {}

    /// Handle Rendering
    fn frame(&mut self, canvas: &mut Canvas, info: &Info, first: bool);

    /// Handle Events  
    /// return true if the event was handled
    fn event(&mut self, event: &WindowEvent) -> EventResult;

    /// Runs the App  
    ///
    /// continuous: draw continuously or after an event
    /// title: the title of the window
    fn run(self, settings: settings::RunSettings)
    where
        Self: Sized + 'static,
    {
        let event_loop = EventLoop::new();
        self.run_with_event_loop(event_loop, settings);
    }

    /// Runs the App
    fn run_with_event_loop(mut self, event_loop: EventLoop<()>, settings: settings::RunSettings)
    where
        Self: Sized + 'static,
    {
        let window = WindowBuilder::new().build(&event_loop).unwrap();
        window.set_title(settings.title);
        let mut state: State = pollster::block_on(State::new(window, &mut self));

        event_loop.run(move |event, _, control_flow| match event {
            Event::RedrawRequested(window_id) if window_id == state.window().id() => {
                match state.frame(&mut self) {
                    Ok(_) => {}
                    Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                    Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    Err(e) => eprintln!("{:?}", e),
                }
            }
            Event::MainEventsCleared => {
                if !settings.continuous {
                    control_flow.set_wait();
                }

                state.window().request_redraw();
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == state.window().id() => match state.event(&mut self, event) {
                EventResult::Unhandled => match event {
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    }
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        state.resize(**new_inner_size);
                    }
                    _ => {}
                },
                EventResult::Handled => {}
                EventResult::Exit => *control_flow = ControlFlow::Exit,
            },
            Event::Resumed => {
                state.create_surface();
            }
            Event::Suspended => {}

            _ => {}
        });
    }
}
