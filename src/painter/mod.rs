use std::{f32::consts::TAU, sync::Arc};

use ab_glyph::{Font, Glyph, ScaleFont};
use wgpu::BindGroup;

use crate::{
    canvas::{draw::Draw, font, Canvas, Shape},
    vertex::{vertex, Vertex},
};

/// TODO:
/// Create a painter from canvas
/// and draw things using it,
/// using canvas to calculate x, y from pixels, screen, etc

pub struct Painter {}

impl Painter {
    pub fn new() -> Self {
        Self {}
    }

    pub fn ellipse(
        &mut self,
        pos: (f32, f32),
        radius: (f32, f32),
        res: u32,
        draw: Draw,
        canvas: &mut Canvas,
    ) -> Arc<Shape> {
        if res < 3 {
            panic!("Failed to draw ellipse, Resolution can't be less then 3");
        }

        let (clr, texture) = match draw {
            Draw::Color(clr) => (clr, None),
            Draw::Texture { texture, color } => (color, Some(texture)),
        };

        let clr = [clr[0] as f32, clr[1] as f32, clr[2] as f32];
        let mut vertices: Vec<Vertex> = vec![vertex([pos.0, pos.1, 0.0], clr, [0.0, 0.0])];
        let mut indices: Vec<u16> = Vec::new();

        for i in 0..res {
            let fi = (i as f32 / res as f32) * TAU;
            let x = fi.cos() * radius.0 + pos.0;
            let y = fi.sin() * radius.1 + pos.1;
            vertices.push(vertex([x, y, 0.0], clr, [0.0, 0.0])); // TODO: fix texture coords
        }

        for i in 1..vertices.len() {
            indices.push(i as u16);
            indices.push(if i + 1 == vertices.len() {
                1
            } else {
                (i + 1) as u16
            });
            indices.push(0);
        }

        self.raw(vertices, indices, texture, canvas)
    }

    pub fn rect(
        &mut self,
        pos0: (f32, f32),
        pos1: (f32, f32),
        draw: Draw,
        canvas: &mut Canvas,
    ) -> Arc<Shape> {
        let (clr, texture) = match draw {
            Draw::Color(clr) => (clr, None),
            Draw::Texture { texture, color } => (color, Some(texture)),
        };

        let clr = [clr[0] as f32, clr[1] as f32, clr[2] as f32];
        self.raw(
            vec![
                vertex([pos0.0, pos0.1, 0.0], clr, [0.0, 1.0]),
                vertex([pos1.0, pos0.1, 0.0], clr, [1.0, 1.0]),
                vertex([pos1.0, pos1.1, 0.0], clr, [1.0, 0.0]),
                vertex([pos0.0, pos1.1, 0.0], clr, [0.0, 0.0]),
            ],
            vec![0, 1, 2, 2, 3, 0],
            texture,
            canvas,
        )
    }

    pub fn line(
        &mut self,
        pos0: (f32, f32),
        pos1: (f32, f32),
        width: f32,
        draw: Draw,
        canvas: &mut Canvas,
    ) -> Arc<Shape> {
        let (clr, texture) = match draw {
            Draw::Color(clr) => (clr, None),
            Draw::Texture { texture, color } => (color, Some(texture)),
        };

        let dist = ((pos1.0 - pos0.0).powf(2.0) + (pos1.1 - pos0.1).powf(2.0)).sqrt();
        let (nx, ny) = (
            (pos0.1 - pos1.1) / dist * width,
            -(pos0.0 - pos1.0) / dist * width,
        );
        let clr = [clr[0] as f32, clr[1] as f32, clr[2] as f32];
        self.raw(
            vec![
                vertex([pos0.0 + nx, pos0.1 + ny, 0.0], clr, [0.0, 1.0]),
                vertex([pos0.0 - nx, pos0.1 - ny, 0.0], clr, [1.0, 1.0]),
                vertex([pos1.0 - nx, pos1.1 - ny, 0.0], clr, [1.0, 0.0]),
                vertex([pos1.0 + nx, pos1.1 + ny, 0.0], clr, [0.0, 0.0]),
            ],
            vec![0, 1, 2, 2, 3, 0],
            texture,
            canvas,
        )
    }

    /// Draws text on to a rect
    pub fn text(
        &mut self,
        pos: (f32, f32),
        scale: f32,
        text: String,
        clr: [f64; 4],
        canvas: &mut Canvas,
    ) -> Arc<Shape> {
        let scaled_font = canvas.font.as_scaled(scale);
        let mut glyphs: Vec<Glyph> = Vec::new();
        let (total_width, total_height) = font::handle(scaled_font, (0.0, 0.0), &text, &mut glyphs);

        let mut texture_bytes: Vec<u8> = vec![0; (total_width * total_height * 4) as usize];
        let clr = [
            (clr[0] * 255.0) as u8,
            (clr[1] * 255.0) as u8,
            (clr[2] * 255.0) as u8,
        ];
        for glyph in glyphs {
            if let Some(outlined) = scaled_font.outline_glyph(glyph) {
                let bounds = outlined.px_bounds();

                outlined.draw(|x, y, c| {
                    let x = x + bounds.min.x as u32;
                    let y = y + bounds.min.y as u32;
                    let ind = (x + y * total_width) as usize * 4;
                    texture_bytes[ind] = clr[0];
                    texture_bytes[ind + 1] = clr[1];
                    texture_bytes[ind + 2] = clr[2];
                    texture_bytes[ind + 3] = (c * 255.0) as u8;
                });
            }
        }

        let texture = canvas.create_texture(texture_bytes, (total_width, total_height));
        let size = canvas
            .info
            .from_pixels((total_width as f32, total_height as f32));

        self.rect(
            pos,
            (pos.0 + size.0, pos.1 + size.1),
            Draw::Texture {
                texture,
                color: [1.0, 1.0, 1.0, 1.0],
            },
            canvas,
        )
    }

    pub fn raw(
        &mut self,
        vertices: Vec<Vertex>,
        indices: Vec<u16>,
        texture: Option<BindGroup>,
        canvas: &mut Canvas,
    ) -> Arc<Shape> {
        let vertex_buffer = canvas.create_vertex_buffer(&vertices);
        let index_buffer = canvas.create_index_buffer(&indices);
        let shape = Arc::new(Shape {
            vertex_buffer,
            index_buffer,
            index_count: indices.len() as u32,
            bindgroup: texture,
        });

        canvas.shape(shape.clone());
        shape
    }
}
