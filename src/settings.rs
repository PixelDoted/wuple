pub struct RunSettings {
    pub continuous: bool,
    pub title: &'static str,
}

impl Default for RunSettings {
    fn default() -> Self {
        Self {
            continuous: true,
            title: "Title",
        }
    }
}
