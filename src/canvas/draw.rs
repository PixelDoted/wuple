use wgpu::BindGroup;

pub enum Draw {
    Color([f64; 4]),
    Texture { texture: BindGroup, color: [f64; 4] },
}

impl Draw {
    pub fn get_color(&self) -> &[f64; 4] {
        match self {
            Draw::Color(color) => color,
            Draw::Texture { color, .. } => color,
        }
    }
}

impl From<(f64, f64, f64, f64)> for Draw {
    fn from(value: (f64, f64, f64, f64)) -> Self {
        Self::Color([
            value.0,
            value.1,
            value.2,
            value.3,
        ])
    }
}

impl From<(f64, f64, f64)> for Draw {
    fn from(value: (f64, f64, f64)) -> Self {
        Self::Color([value.0, value.1, value.2, 1.0])
    }
}

impl Into<wgpu::Color> for Draw {
    fn into(self) -> wgpu::Color {
        match self {
            Draw::Color(color) => {
                wgpu::Color {
                    r: color[0],
                    g: color[1],
                    b: color[2],
                    a: color[3],
                }
            },
            Draw::Texture { color, .. } => {
                wgpu::Color {
                    r: color[0],
                    g: color[1],
                    b: color[2],
                    a: color[3],
                }
            },
        }
    }
}