use ab_glyph::{point, Font, Glyph, ScaleFont};

// https://github.com/alexheretic/ab-glyph/blob/main/dev/examples/image.rs
pub fn handle<'a, F, SF>(
    font: SF,
    pos: (f32, f32),
    text: &str,
    target: &mut Vec<Glyph>,
) -> (u32, u32)
where
    F: Font,
    SF: ScaleFont<F>,
{
    let v_advance = font.height() + font.line_gap();
    let mut caret = point(pos.0, pos.1 + font.ascent());
    let mut last_glyph: Option<Glyph> = None;

    let mut total_width = caret.x;
    let mut total_height = font.height();

    for c in text.chars() {
        if c.is_control() {
            if c == '\n' {
                caret = point(pos.0, caret.y + v_advance);
                last_glyph = None;
                total_height = caret.y;
            }
            continue;
        }

        let mut glyph = font.scaled_glyph(c);
        if let Some(previous) = last_glyph.take() {
            caret.x += font.kern(previous.id, glyph.id);
        }

        glyph.position = caret;
        last_glyph = Some(glyph.clone());
        caret.x += font.h_advance(glyph.id);
        if caret.x > total_width {
            total_width = caret.x;
        }

        target.push(glyph);
    }

    (total_width.ceil() as u32, total_height.ceil() as u32)
}
