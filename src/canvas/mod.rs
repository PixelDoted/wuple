pub mod draw;
pub(crate) mod font;
pub mod shape;
pub(crate) mod texture;

pub use shape::Shape;

use std::sync::Arc;

use ab_glyph::FontRef;
use wgpu::{util::DeviceExt, BindGroup, BindGroupLayout, Device, Queue};

use crate::{
    info::Info,
    vertex::Vertex,
};

pub struct MetaCanvas {
    pub(crate) clear_color: wgpu::Color,
    pub(crate) shapes: Vec<Arc<Shape>>,
}

impl MetaCanvas {
    pub fn new() -> Self {
        Self {
            clear_color: wgpu::Color { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
            shapes: Vec::new(),
        }
    }
}

pub struct Canvas<'a> {
    pub(crate) meta: &'a mut MetaCanvas,

    pub(crate) device: &'a mut Device,
    pub(crate) queue: &'a mut Queue,
    pub(crate) texture_bind_group_layout: &'a BindGroupLayout,
    pub(crate) font: &'a FontRef<'a>,

    pub(crate) info: &'a Info<'a>,
}

impl<'a> Canvas<'a> {
    pub(crate) fn new(
        meta: &'a mut MetaCanvas,
        device: &'a mut Device,
        queue: &'a mut Queue,
        texture_bind_group_layout: &'a BindGroupLayout,
        font: &'a FontRef<'a>,

        info: &'a Info,
    ) -> Self {
        Self {
            meta,

            device,
            queue,
            texture_bind_group_layout,
            font,

            info,
        }
    }

    /// Change the Clear Color used
    pub fn clear_color(&mut self, color: [f64; 4]) {
        self.meta.clear_color.r = color[0];
        self.meta.clear_color.g = color[1];
        self.meta.clear_color.b = color[2];
        self.meta.clear_color.a = color[3];
    }

    pub fn shape(&mut self, shape: Arc<Shape>) {
        self.meta.shapes.push(shape);
    }

    pub fn create_vertex_buffer(&mut self, vertices: &[Vertex]) -> wgpu::Buffer {
        self.device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(vertices),
                usage: wgpu::BufferUsages::VERTEX,
            })
    }

    pub fn create_index_buffer(&mut self, indices: &[u16]) -> wgpu::Buffer {
        self.device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Index Buffer"),
                contents: bytemuck::cast_slice(indices),
                usage: wgpu::BufferUsages::INDEX,
            })
    }

    pub fn create_texture(&mut self, bytes: Vec<u8>, size: (u32, u32)) -> BindGroup {
        texture::create(
            self.device,
            self.queue,
            self.texture_bind_group_layout,
            bytes,
            size,
        )
    }
}
