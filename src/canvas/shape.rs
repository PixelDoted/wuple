use wgpu::{BindGroup, Buffer};

pub struct Shape {
    pub(crate) vertex_buffer: Buffer,
    pub(crate) index_buffer: Buffer,
    pub(crate) index_count: u32,
    pub(crate) bindgroup: Option<BindGroup>,
}
