use winit::window::Window;

pub struct Info<'a> {
    pub(crate) window: &'a Window,
}

impl<'a> Info<'a> {
    /// Convert from pixels to units
    pub fn from_pixels(&self, pos: (f32, f32)) -> (f32, f32) {
        let size = self.size();
        (pos.0 / size.0 as f32, pos.1 / size.1 as f32)
    }

    /// Convert from screen space in pixels to world space in pixels
    pub fn from_screen(&self, pos: (f32, f32)) -> (f32, f32) {
        let size = self.size();
        (pos.0 * 2.0 - size.0 as f32, pos.1 * 2.0 + size.1 as f32)
    }

    /// Convert from units to pixels
    pub fn to_pixels(&self, pos: (f32, f32)) -> (f32, f32) {
        let size = self.size();
        (pos.0 * size.0 as f32, pos.1 * size.1 as f32)
    }

    /// Convert from world space in pixels to screen space in pixels
    pub fn to_screen(&self, pos: (f32, f32)) -> (f32, f32) {
        let size = self.size();
        ((pos.0 + size.0 as f32) * 0.5, (pos.1 - size.1 as f32) * 0.5)
    }

    /// Get the window size
    pub fn size(&self) -> (u32, u32) {
        self.window.inner_size().into()
    }

    pub fn set_title(&self, title: &str) {
        self.window.set_title(title);
    }

    pub fn set_cursor_visible(&self, visible: bool) {
        self.window.set_cursor_visible(visible);
    }
}
