#[cfg(target_os = "android")]
pub const BACKENDS: wgpu::Backends = unsafe {
    wgpu::Backends::from_bits_unchecked(
        wgpu::Backends::BROWSER_WEBGPU.bits() | wgpu::Backends::GL.bits(),
    )
};

#[cfg(not(target_os = "android"))]
pub const BACKENDS: wgpu::Backends = wgpu::Backends::all();

pub fn limits() -> wgpu::Limits {
    if cfg!(target_arch = "wasm32") {
        return wgpu::Limits::downlevel_webgl2_defaults();
    } else {
        #[cfg(target_os = "android")]
        return wgpu::Limits {
            max_compute_workgroup_size_x: 128,
            max_compute_workgroup_size_y: 128,
            max_compute_invocations_per_workgroup: 128,
            max_uniform_buffer_binding_size: 16384,
            max_storage_textures_per_shader_stage: 0,
            max_storage_buffers_per_shader_stage: 0,
            max_dynamic_storage_buffers_per_pipeline_layout: 0,
            ..Default::default()
        };

        #[cfg(not(target_os = "android"))]
        return wgpu::Limits::default();
    }
}
