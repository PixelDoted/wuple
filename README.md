# Wuple
[docs](https://docs.rs/wuple) [crates.io](https://crates.io/wuple)  
Simple, Preformant rendering on WGPU

[Examples](./wuple_examples)

## Todos
- [ ] Performance Optimizations
- [ ] Switch to `cosmic-text`
