use wuple::winit::event_loop::EventLoop;

mod examples {
    use wuple::winit::event_loop::EventLoop;

    mod basic;
    mod ellipse;
    mod image;
    mod raw_shape;
    mod text_scroll;
    mod touch;

    pub fn impl_main(event_loop: EventLoop<()>) {
        if cfg!(feature = "basic") {
            basic::impl_main(event_loop);
        } else if cfg!(feature = "text_scroll") {
            text_scroll::impl_main(event_loop);
        } else if cfg!(feature = "raw_shape") {
            raw_shape::impl_main(event_loop);
        } else if cfg!(feature = "ellipse") {
            ellipse::impl_main(event_loop);
        } else if cfg!(feature = "image") {
            image::impl_main(event_loop);
        } else if cfg!(feature = "touch") {
            touch::impl_main(event_loop);
        }
    }
}

pub fn main(event_loop: EventLoop<()>) {
    examples::impl_main(event_loop);
}
