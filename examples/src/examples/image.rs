use std::sync::Arc;

use image::EncodableLayout;
use wuple::{
    canvas::{draw::Draw, Canvas, Shape},
    info::Info,
    painter::Painter,
    settings::RunSettings,
    App, EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    Example { texture_rect: None }.run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Image Example",
        },
    );
}

pub struct Example {
    texture_rect: Option<Arc<Shape>>,
}

impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, _info: &Info, first: bool) {
        if first {
            let mut painter = Painter::new();

            let image = image::load_from_memory(include_bytes!("brick_texture.png")).unwrap();
            let image = image.as_rgba8().unwrap();
            let dimensions = image.dimensions();
            let texture = canvas.create_texture(image.as_bytes().to_vec(), dimensions);
            self.texture_rect = Some(painter.rect(
                (-0.5, -0.5),
                (0.5, 0.5),
                Draw::Texture {
                    texture,
                    color: [0.0, 0.0, 0.0, 1.0],
                },
                canvas,
            ));
        } else {
            canvas.shape(self.texture_rect.clone().unwrap());
        }
    }

    fn event(&mut self, _event: &wuple::winit::event::WindowEvent) -> EventResult {
        EventResult::Unhandled
    }
}
