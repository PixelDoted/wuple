use winit::event::WindowEvent;
use wuple::{
    canvas::{draw::Draw, Canvas},
    info::Info,
    painter::Painter,
    settings::RunSettings,
    App, EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    Example { res: 3 }.run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Ellipse Example",
        },
    );
}

struct Example {
    res: u32,
}
impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, info: &Info, _first: bool) {
        let mut painter = Painter::new();

        let radius = info.from_pixels((300.0, 300.0));
        painter.ellipse(
            (0.0, 0.0),
            radius,
            self.res,
            Draw::Color([1.0, 1.0, 1.0, 1.0]),
            canvas,
        );
    }

    fn event(&mut self, event: &wuple::winit::event::WindowEvent) -> EventResult {
        match event {
            WindowEvent::MouseWheel { delta, .. } => match delta {
                winit::event::MouseScrollDelta::LineDelta(_, y) => {
                    if *y > 0.0 {
                        self.res += 1;
                    } else {
                        self.res -= 1;
                    }

                    self.res = self.res.max(3);
                    return EventResult::Handled;
                }
                _ => (),
            },
            _ => (),
        }

        EventResult::Unhandled
    }
}
