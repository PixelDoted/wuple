use wuple::{
    canvas::Canvas, info::Info, painter::Painter, settings::RunSettings, vertex::vertex, App,
    EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    Example {}.run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Raw Shape Example",
        },
    );
}

pub struct Example {}
impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, _info: &Info, _first: bool) {
        let mut painter = Painter::new();

        // Draw Triangle
        painter.raw(
            vec![
                vertex([0.0, 0.5, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0]),
                vertex([-0.5, -0.5, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0]),
                vertex([0.5, -0.5, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0]),
            ],
            vec![0, 1, 2],
            None,
            canvas,
        );
    }

    fn event(&mut self, _event: &wuple::winit::event::WindowEvent) -> EventResult {
        EventResult::Unhandled
    }
}
