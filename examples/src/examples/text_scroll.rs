use std::time::Instant;

use winit::event::WindowEvent;
use wuple::{
    canvas::Canvas, info::Info, painter::Painter, settings::RunSettings, App,
    EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    let mut text: String = String::new();
    for i in 0..100 {
        // This can be optimized by only rendering the required lines/characters
        text.push_str(&format!(
            "{}: the quick brown fox jumped over the lazy dog\n",
            i
        ));
    }

    Example {
        scroll: 0.0,
        text,

        last_frame: Instant::now(),
    }
    .run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Text Scroll Example",
        },
    );
}

pub struct Example {
    scroll: f32,
    text: String,

    last_frame: Instant,
}

impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, _info: &Info, _first: bool) {
        let mut painter = Painter::new();

        let fps = 1.0 / self.last_frame.elapsed().as_secs_f32();
        self.last_frame = Instant::now();

        painter.text((0.0, 0.0), 32.0, format!("FPS: {}", fps), [1.0, 1.0, 1.0, 1.0], canvas);
        painter.text((0.0, self.scroll), 32.0, self.text.clone(), [1.0, 1.0, 1.0, 1.0], canvas);
    }

    fn event(&mut self, event: &wuple::winit::event::WindowEvent) -> EventResult {
        match event {
            WindowEvent::MouseWheel { delta, .. } => match delta {
                winit::event::MouseScrollDelta::LineDelta(_x, y) => {
                    self.scroll(*y * 0.01);
                    return EventResult::Handled;
                }
                _ => (),
            },
            _ => (),
        }

        EventResult::Unhandled
    }
}

impl Example {
    pub fn scroll(&mut self, delta: f32) {
        self.scroll += delta;
        self.scroll = self.scroll.min(0.0);
    }
}
