use std::time::Instant;

use wuple::{
    canvas::{draw::Draw, Canvas},
    info::Info,
    painter::Painter,
    settings::RunSettings,
    App, EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    Example {
        position: (0.0, 0.0),
        velocity: (0.01, 0.02),
        last_frame: Instant::now(),
    }
    .run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Basic Example",
        },
    );
}

pub struct Example {
    pub position: (f32, f32),
    pub velocity: (f32, f32),
    pub last_frame: Instant,
}

impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, _info: &Info, _first: bool) {
        let mut painter = Painter::new();
        let fps = 1.0 / self.last_frame.elapsed().as_secs_f32();
        self.last_frame = Instant::now();

        painter.text(
            (0.0, 0.0),
            32.0,
            format!("FPS: {}", fps),
            [1.0, 1.0, 1.0, 1.0],
            canvas,
        );

        self.position.0 += self.velocity.0;
        self.position.1 += self.velocity.1;

        if self.position.0 - 0.1 < -1.0 || self.position.0 + 0.1 > 1.0 {
            self.velocity.0 *= -1.0;
        }
        if self.position.1 - 0.1 < -1.0 || self.position.1 + 0.1 > 1.0 {
            self.velocity.1 *= -1.0;
        }

        painter.rect(
            (self.position.0 - 0.1, self.position.1 - 0.1),
            (self.position.0 + 0.1, self.position.1 + 0.1),
            Draw::Color([0.5, 0.5, 0.5, 1.0]),
            canvas,
        );
    }

    fn event(&mut self, _event: &wuple::winit::event::WindowEvent) -> EventResult {
        EventResult::Unhandled
    }
}
