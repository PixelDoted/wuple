use std::{collections::HashMap, time::Instant};

use winit::event::WindowEvent;
use wuple::{
    canvas::{draw::Draw, Canvas},
    info::Info,
    painter::Painter,
    settings::RunSettings,
    App, EventResult,
};

pub fn impl_main(event_loop: winit::event_loop::EventLoop<()>) {
    Example {
        last_frame: Instant::now(),
        mouse_position: (0.0, 0.0),
        touches: HashMap::new(),
    }
    .run_with_event_loop(
        event_loop,
        RunSettings {
            continuous: true,
            title: "Touch Example",
        },
    );
}

pub struct Example {
    pub last_frame: Instant,
    pub mouse_position: (f32, f32),
    pub touches: HashMap<u64, (f32, f32)>,
}

impl App for Example {
    fn frame(&mut self, canvas: &mut Canvas, info: &Info, _first: bool) {
        let mut painter = Painter::new();

        let fps = 1.0 / self.last_frame.elapsed().as_secs_f32();
        self.last_frame = Instant::now();

        for (_id, pos) in &self.touches {
            let radius = info.from_pixels((128.0, 128.0));
            painter.ellipse(
                info.from_pixels(info.from_screen(*pos)),
                radius,
                64,
                Draw::Color([1.0, 0.0, 0.0, 1.0]),
                canvas,
            );
        }

        painter.text(
            info.from_pixels(info.from_screen((5.0, -20.0))),
            32.0,
            format!("FPS: {}", fps),
            [1.0, 1.0, 1.0, 1.0],
            canvas,
        );
    }

    fn event(&mut self, event: &wuple::winit::event::WindowEvent) -> EventResult {
        match event {
            WindowEvent::CursorMoved { position, .. } => {
                self.mouse_position = (position.x as f32, -position.y as f32);
                if let Some(touch) = self.touches.get_mut(&0) {
                    *touch = self.mouse_position;
                }
            }
            WindowEvent::Touch(touch) => {
                let pos = (touch.location.x as f32, -touch.location.y as f32);
                let id = touch.id + 1;
                match touch.phase {
                    winit::event::TouchPhase::Started => {
                        self.touches.insert(id, pos);
                    }
                    winit::event::TouchPhase::Moved => {
                        *self.touches.get_mut(&id).unwrap() = pos;
                    }
                    winit::event::TouchPhase::Ended => {
                        self.touches.remove(&id);
                    }
                    winit::event::TouchPhase::Cancelled => {
                        self.touches.remove(&id);
                    }
                }
            }
            WindowEvent::MouseInput { state, .. } => match state {
                winit::event::ElementState::Pressed => {
                    self.touches.insert(0, self.mouse_position);
                }
                winit::event::ElementState::Released => {
                    self.touches.remove(&0);
                }
            },
            _ => (),
        }

        EventResult::Unhandled
    }
}
