use winit::event_loop::EventLoopBuilder;

fn main() {
    env_logger::init();
    let event_loop = EventLoopBuilder::new().build();
    wuple_examples::main(event_loop);
}
